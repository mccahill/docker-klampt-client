FROM ubuntu:16.04

MAINTAINER Mark McCahill "mark.mccahill@duke.edu"

USER root

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    curl \
    wget \
    bzip2 \
    bc \
    ca-certificates \
    sudo \
    locales \
    git \
    vim \
    less \
    gcc \
    g++ \
    make \
    build-essential \
    python-dev \
    python-scipy \
    libssl-dev \
    unzip \
 && apt-get clean 

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Install Tini
RUN wget --quiet https://github.com/krallin/tini/releases/download/v0.10.0/tini && \
    echo "1361527f39190a7338a0b434bd8c88ff7233ce7b9a4876f3315c22fce7eca1b0 *tini" | sha256sum -c - && \
    mv tini /usr/local/bin/tini && \
    chmod +x /usr/local/bin/tini

# install a recent version of nodejs 
RUN curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash - ; \
    apt-get install -y nodejs
    
# Configure environment
ENV SHELL /bin/bash
ENV NB_USER student
ENV NB_UID 1000
ENV HOME /home/$NB_USER
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Create student user with UID=1000 and in the 'users' group
RUN useradd -m -s /bin/bash -N -u $NB_UID $NB_USER 

ADD express-web-client /usr/local/src/express-web-client
#RUN cp -r /express-web-client /usr/local/src/express-web-client
RUN chown -R $NB_USER /usr/local/src/express-web-client

USER $NB_USER

# build the node.js application
RUN cd /usr/local/src/express-web-client ; \
    npm install 

# remove the npm build artifacts 
RUN rm -rf /home/student/.npm
RUN rm -rf /home/student/.node-gyp

USER $NB_USER

USER root
RUN chown -R $NB_USER:users /home/$NB_USER 
    
# expose the express-web-client port
EXPOSE 3000

# script to start the nterm.js node app
COPY  start-express-web-client.sh   /usr/local/bin/start-express-web-client.sh

USER $NB_USER
ENV HOME /home/$NB_USER

USER  root
# Configure container startup
ENTRYPOINT ["tini", "--"]
CMD ["start-express-web-client.sh"]

# Switch back to student to avoid accidental container runs as root
USER $NB_USER
WORKDIR /home/$NB_USER
ENV HOME /home/$NB_USER
