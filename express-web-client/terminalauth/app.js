var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);
var os = require('os');
var fs = require('fs');


var passport = require('passport');
var Strategy = require('passport-local').Strategy;

var users = []

// Configure the local strategy for use by Passport.
//
// The local strategy require a `verify` function which receives the credentials
// (`username` and `password`) submitted by the user.  The function must verify
// that the password is correct and then invoke `cb` with a user object, which
// will be set at `req.user` in route handlers after authentication.
passport.use(new Strategy({
    usernameField: 'username',
    passwordField: 'token',
    session: false
  },
  function(username, password, cb) {
    console.log('checking password: ' + password ); 

    // the environmental variable NICETOKEN will be passed into the docker container
    // so this is what we check the password against

    var a_nice_token = process.env.NICETOKEN;

    if ( password == a_nice_token ) {
      // the generic user in the docker container is 'student'
      return cb(null, 'student');
    } else {
      return cb(null, false);
    }
  }));


passport.serializeUser(function(user, cb) {
  var id = users.length + 1
  users[id] = user
  console.log('serializing ' + user + ' with id ' + id );
  cb(null, id);
});

passport.deserializeUser(function(id, cb) {
    console.log('deserializing id ' + id + ' as user ' + users[id]  );
    cb(null, users[id] );
});


var logs = {};


// Initialize Passport and restore authentication state, if any, from the session.
app.use(passport.initialize());
app.use(passport.session());

app.use('/build', express.static(__dirname + '/../build'));


app.get('/loginfail',
  function(req, res) {
  res.status(400);
  res.send('Unauthorized - bad token');
});


app.get('/',
  passport.authenticate('local', { failureRedirect: '/loginfail' }),
  function(req, res){
    myindex = fs.readFileSync(__dirname + '/index.html').toString();
    res.setHeader('Content-Type', 'text/html');
    res.send(myindex);
});


app.get('/index_debug.html',
  passport.authenticate('local', { failureRedirect: '/loginfail' }),
  function(req, res){
    myindex = fs.readFileSync(__dirname + '/index_debug.html').toString();
    res.setHeader('Content-Type', 'text/html');
    res.send(myindex);
});


app.get('/index_debug',
  passport.authenticate('local', { failureRedirect: '/loginfail' }),
  function(req, res){
    myindex = fs.readFileSync(__dirname + '/index_debug.html').toString();
    res.setHeader('Content-Type', 'text/html');
    res.send(myindex);
});


app.get('/index_debug_testserver',
  passport.authenticate('local', { failureRedirect: '/loginfail' }),
  function(req, res){
    myindex = fs.readFileSync(__dirname + '/index_debug_testserver.html').toString();
    res.setHeader('Content-Type', 'text/html');
    res.send(myindex);
});


app.get('/index_testserver',
  passport.authenticate('local', { failureRedirect: '/loginfail' }),
  function(req, res){
    myindex = fs.readFileSync(__dirname + '/index_testserver.html').toString();
    res.setHeader('Content-Type', 'text/html');
    res.send(myindex);
});


app.get('/style.css', function(req, res){
  res.sendFile(__dirname + '/style.css');
});

app.get('/main.js', function(req, res){
  res.sendFile(__dirname + '/main.js');
});


app.use("/Scenarios", express.static(__dirname + '/Scenarios'));
app.use("/images", express.static(__dirname + '/images'));
app.use("/w2ui", express.static(__dirname + '/w2ui'));
app.use("/three.js", express.static(__dirname + '/three.js'));
app.use("/ace-builds", express.static(__dirname + '/ace-builds'));


app.get('/mode-python.js', function(req, res){
  res.sendFile(__dirname + '/mode-python.js');
});

app.get('/KlamptClient.js', function(req, res){
  res.sendFile(__dirname + '/KlamptClient.js');
});



app.post('/terminals', function (req, res) {
  passport.authenticate('local', { failureRedirect: '/loginfail' });
  res.sendFile(__dirname + '/index.html');
});


var port = process.env.PORT || 3000,
    host = os.platform() === 'win32' ? '127.0.0.1' : '0.0.0.0';

console.log('App listening to http://' + host + ':' + port);
app.listen(port, host);
